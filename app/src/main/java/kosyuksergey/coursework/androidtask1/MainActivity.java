package kosyuksergey.coursework.androidtask1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private boolean divisionFlag = false;
    private boolean resultFlag = true;

    @BindView(R.id.first_radio_group)
    RadioGroup firstGroup;
    @BindView(R.id.second_radio_group)
    RadioGroup secondGroup;

    @BindView(R.id.edit1)
    EditText firstEdit;
    @BindView(R.id.edit2)
    EditText secondEdit;

    @BindView(R.id.use_float)
    CheckBox useFloatCheckBox;
    @BindView(R.id.use_signed)
    CheckBox useSignedCheckBox;

    @BindView(R.id.result)
    TextView resultTextView;

    @BindString(R.string.clear)
    String clearTitle;
    @BindString(R.string.clear_text)
    String clearText;
    @BindString(R.string.yes)
    String yesText;
    @BindString(R.string.no)
    String noText;
    @BindString(R.string.division_by_zero)
    String divisionByZeroText;
    @BindString(R.string.result)
    String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if(result.isEmpty())
            result = getString(R.string.result);

        //resultTextView.setText(result);
        firstGroup.setOnCheckedChangeListener(firstListener);
        secondGroup.setOnCheckedChangeListener(secondListener);
        useFloatCheckBox.setChecked(true);
        useSignedCheckBox.setChecked(true);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("result", resultTextView.getText().toString());
        savedInstanceState.putBoolean("divisionFlag", divisionFlag);
        savedInstanceState.putBoolean("resultFlag", resultFlag);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        divisionFlag = savedInstanceState.getBoolean("divisionFlag");
        resultFlag = savedInstanceState.getBoolean("resultFlag");
        if (divisionFlag)
            resultTextView.setText(divisionByZeroText);
        else if (resultFlag)
            resultTextView.setText(result);
        else
            resultTextView.setText(savedInstanceState.getString("result"));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mMenuInflater = getMenuInflater();
        mMenuInflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private RadioGroup.OnCheckedChangeListener firstListener =
            new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                secondGroup.setOnCheckedChangeListener(null);
                secondGroup.clearCheck();
                secondGroup.setOnCheckedChangeListener(secondListener);
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener secondListener =
            new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId != -1) {
                firstGroup.setOnCheckedChangeListener(null);
                firstGroup.clearCheck();
                firstGroup.setOnCheckedChangeListener(firstListener);
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.clear:
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle(clearTitle);
                alertDialog.setMessage(clearText);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, yesText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                clearData();
                                dialog.dismiss();
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, noText,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void clearData() {
        firstGroup.clearCheck();
        secondGroup.clearCheck();
        firstEdit.getText().clear();
        secondEdit.getText().clear();
        useFloatCheckBox.setChecked(false);
        useSignedCheckBox.setChecked(false);
        resultTextView.setText(result);
        resultFlag = true;
    }

    @OnCheckedChanged({R.id.use_float, R.id.use_signed})
    public void onRadioButtonClicked(CheckBox checkBox) {
        boolean checked = checkBox.isChecked();

        switch (checkBox.getId()) {
            case R.id.use_float:
                if (checked) {
                    firstEdit.setInputType(firstEdit.getInputType()
                            | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    secondEdit.setInputType(firstEdit.getInputType()
                            | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                } else {
                    if (useSignedCheckBox.isChecked()) {
                        firstEdit.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED
                                | InputType.TYPE_CLASS_NUMBER);
                        secondEdit.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED
                                | InputType.TYPE_CLASS_NUMBER);
                        firstEdit.setText("");
                        secondEdit.setText("");
                    } else {
                        firstEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
                        secondEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
                        firstEdit.setText("");
                        secondEdit.setText("");
                    }
                }
                break;
            case R.id.use_signed:
                if (checked) {
                    firstEdit.setInputType(firstEdit.getInputType()
                            | InputType.TYPE_NUMBER_FLAG_SIGNED);
                    secondEdit.setInputType(firstEdit.getInputType()
                            | InputType.TYPE_NUMBER_FLAG_SIGNED);
                } else {
                    if (useFloatCheckBox.isChecked()) {
                        firstEdit.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL
                                | InputType.TYPE_CLASS_NUMBER);
                        secondEdit.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL
                                | InputType.TYPE_CLASS_NUMBER);
                        firstEdit.setText("");
                        secondEdit.setText("");
                    } else {
                        firstEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
                        secondEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
                        firstEdit.setText("");
                        secondEdit.setText("");
                    }
                }
                break;
        }
    }

    @OnClick(R.id.calculate)
    public void calculate(Button button) {
        int chkId1 = firstGroup.getCheckedRadioButtonId();
        int chkId2 = secondGroup.getCheckedRadioButtonId();
        int realCheck = chkId1 == -1 ? chkId2 : chkId1;
        double firstDigit = 0;
        double secondDigit = 0;

        try {
            firstDigit = Double.parseDouble(firstEdit.getText().toString());
            secondDigit = Double.parseDouble(secondEdit.getText().toString());
        } catch (Throwable e) {

        }
        switch (realCheck) {
            case R.id.plus:
                resultTextView.setText(Double.toString(firstDigit + secondDigit));
                resultFlag = false;
                break;
            case R.id.minus:
                resultTextView.setText(Double.toString(firstDigit - secondDigit));
                resultFlag = false;
                break;
            case R.id.multiple:
                resultTextView.setText(Double.toString(firstDigit * secondDigit));
                resultFlag = false;
                break;
            case R.id.divide:
                if (secondDigit == 0) {
                    resultTextView.setText(result);
                    divisionFlag = true;
                    resultFlag = false;
                }
                else {
                    resultFlag = false;
                    resultTextView.setText(Double.toString(firstDigit / secondDigit));
                }
                break;
            default:
                Toast.makeText(this, R.string.operation_not_found, Toast.LENGTH_SHORT).show();
                break;
        }
    }

}